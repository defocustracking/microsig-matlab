function I = take_image(mic,P)

I = zeros(mic.pixel_dim_y,mic.pixel_dim_x);

dp_s = unique(P(:,4));
if size(P,2)<6
    input_case = double(length(dp_s)~=1) + 2*double(size(P,2)==5);
elseif size(P,2)==7; input_case = 4;
elseif size(P,2)==8; input_case = 5;
else
    return
end

switch input_case
    case 0 % monodispersed, intensity constant
        n_points = round(mic.points_per_pixel*2*pi*...
            (dp_s*mic.magnification/mic.pixel_size)^2);
        xp = create_particle(dp_s,n_points,mic.n_rays);
        for ii = 1:size(P,1)
            Id = image_spherical(mic,xp,P(ii,1:3));
            I = I + Id;
        end
        
    case 1 % polydispersed, intensity constant
        for ii = 1:size(P,1)
            n_points = round(mic.points_per_pixel*2*pi*...
                (P(ii,4)*mic.magnification/mic.pixel_size)^2);
            xp = create_particle(P(ii,4),n_points,mic.n_rays);
            Id = image_spherical(mic,xp,P(ii,1:3));
            I = I + Id;
        end
        
    case 2 % monodispersed, intensity not constant
        n_points = round(mic.points_per_pixel*2*pi*...
            (dp_s*mic.magnification/mic.pixel_size)^2);
        xp = create_particle(dp_s,n_points,mic.n_rays);
        for ii = 1:size(P,1)
            Id = image_spherical(mic,xp,P(ii,1:3));
            I = I + Id*P(ii,5);
        end
        
    case 3 % polydispersed, intensity not constant
        for ii = 1:size(P,1)
            n_points = round(mic.points_per_pixel*2*pi*...
                (P(ii,4)*mic.magnification/mic.pixel_size)^2);
            xp = create_particle(P(ii,4),n_points,mic.n_rays);
            Id = image_spherical(mic,xp,P(ii,1:3));
            I = I + Id*P(ii,5);
        end
        
    case 4 % ellipsoids, intensity constant
       for ii = 1:size(P,1)
            n_points = round(mic.points_per_pixel*2*pi*...
                (P(ii,4)*mic.magnification/mic.pixel_size)^2);         
            ecc = P(ii,5);
            if ecc>1
                fact = 1/2*(1+ecc/sqrt(1-1/ecc^2)*asin(sqrt(1-1/ecc^2))); % area elipsoid/area sphere
                n_points = round(fact*n_points);
            elseif ecc<1
                fact = 1/2*(1+ecc^2/sqrt(1-ecc^2)*atan(sqrt(1-ecc^2))); % area elipsoid/area sphere
                n_points = round(fact*n_points);
            end       
            xp = create_ellipsoid(P(ii,4:7),n_points,mic.n_rays);
            Id = image_spherical(mic,xp,P(ii,1:3));
            I = I + Id;
        end
           
    case 5 % ellipsoids, intensity not constant
        for ii = 1:size(P,1)
       n_points = round(mic.points_per_pixel*2*pi*...
                (P(ii,4)*mic.magnification/mic.pixel_size)^2);         
            ecc = P(ii,5);
            if ecc>1
                fact = 1/2*(1+ecc/sqrt(1-1/ecc^2)*asin(sqrt(1-1/ecc^2))); % area elipsoid/area sphere
                n_points = round(fact*n_points);
            elseif ecc<1
                fact = 1/2*(1+ecc^2/sqrt(1-ecc^2)*atan(sqrt(1-ecc^2))); % area elipsoid/area sphere
                n_points = round(fact*n_points);
            end       
            xp = create_ellipsoid(P(ii,4:7),n_points,mic.n_rays);
            Id = image_spherical(mic,xp,P(ii,1:3));
            I = I + Id*P(ii,8);
        end
end

I = I*mic.gain;
if mic.background_mean~=0 
%     I(I<mic.background_mean) = mic.background_mean; 
    I = I+mic.background_mean; 
end
if mic.background_noise~=0
    I = round(I+random('norm',0,mic.background_noise,...
        mic.pixel_dim_y,mic.pixel_dim_x));
end


function I = image_spherical(this,xp,P1)

% take image of a particle with a spherical lens

lens_radius = tan(asin(this.numerical_aperture))*(1+1/this.magnification)*this.focal_length;
% distance lens-ccd
dCCD = -this.focal_length*(this.magnification+1);
% distance particle-lens
dPART = P1(3)+this.focal_length*(1/this.magnification+1);

% linear transformation from the object plane to the lens plane
T2 = [1 0 dPART 0; 0 1 0 dPART; 0 0 1 0; 0 0 0 1];
% light field right before the lens
x = T2^-1*xp;

% remove rays outside of the lens aperture
ind = x(1,:).^2+x(2,:).^2<=lens_radius.^2;
x = x(:,ind);

% transformation of the light field with spherical lens
a = x(1,:); b = x(2,:);
c = x(3,:); d = x(4,:);
% radius of curvature of the lens
rk = this.focal_length*(this.ri_lens/this.ri_medium-1)*2;
dum = a*0;
% refraction medium-lens
% ray-vector befor lens
Vr = [1+dum; c; d]; Vr = Vr./repmat(sqrt(sum(Vr.^2)),3,1);
% normal-vector to the lens surface
VL = [rk+dum; a; b]; VL = VL./repmat(sqrt(sum(VL.^2)),3,1);
% tangent-vector to the lens surface
Vrot = cross(Vr,VL);
Vrot = cross(Vrot,VL);
Vrot = Vrot./repmat(sqrt(sum(Vrot.^2)),3,1);
% angle after snell-law correction
vx = dot(Vr,VL);
vy = dot(Vr,Vrot);
th11 = asin(this.ri_medium/this.ri_lens*sin(atan(vy./vx)));
% new ray-vector inside the lens
Vr11 = VL.*repmat(cos(th11),3,1)+Vrot.*repmat(sin(th11),3,1);
Vr = Vr11./repmat(Vr11(1,:),3,1);
% refraction lens-medium
% normal-vector to the lens surface
VL = [VL(1,:); -VL(2:3,:)];
% tangent-vector to the lens surface
Vrot = cross(Vr,VL);
Vrot = cross(Vrot,VL);
Vrot = Vrot./repmat(sqrt(sum(Vrot.^2)),3,1);
% angle after snell-law correction
vx = dot(Vr,VL);
vy = dot(Vr,Vrot);
th11 = asin(this.ri_lens/this.ri_medium*sin(atan(vy./vx)));
% new ray-vector outside the lens
Vr11 = VL.*repmat(cos(th11),3,1)+Vrot.*repmat(sin(th11),3,1);
Vr = Vr11./repmat(Vr11(1,:),3,1);
% light field after the spherical lens
x(3,:) = Vr(2,:);
x(4,:) = Vr(3,:);

if this.cyl_focal_length==0
%     linear transformation from the lens plane to the ccd plane
    T1 = [1 0 -dCCD 0; 0 1 0 -dCCD; 0 0 1 0; 0 0 0 1];
%     light field at the ccd plane
    xs = T1^-1*x;
else
%     linear transformation from the lens plane to the cyl_lens plane
    T1c = [1 0 -dCCD*1/3 0; 0 1 0 -dCCD*1/3; 0 0 1 0; 0 0 0 1];
%     light field at the cylindrical lens plane
    xc = T1c^-1*x;
%     light field after the cylindrical lens plane
    Tc = [1 0 0 0; 0 1 0 0; -1/this.cyl_focal_length 0 1 0; 0 0 0 1];
    xc_a = Tc^-1*xc;
%     light field at the ccd plane
    T1 = [1 0 -dCCD*2/3 0; 0 1 0 -dCCD*2/3; 0 0 1 0; 0 0 0 1];
%     light field at the ccd plane
    xs = T1^-1*xc_a;
end

% transform the position in pixel units
X = round(xs(1,:)/this.pixel_size+P1(1));
Y = round(xs(2,:)/this.pixel_size+P1(2));

% remove rays outside the CCD
ind = X>0 & X<=this.pixel_dim_x & Y>0 & Y<=this.pixel_dim_y &...
    imag(X)==0 & imag(Y)==0;

% count number of rays in each pixel
countXY = sort(Y(ind) + (X(ind)-1)*this.pixel_dim_y);
[indi, ia] = unique(countXY);
nCounts = diff([ia; length(countXY)+1]);

% prepare image
I = zeros(this.pixel_dim_y,this.pixel_dim_x);
I(indi) = nCounts;


