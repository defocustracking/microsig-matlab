# MicroSIG
Synthetic image generator (SIG) for defocused/astigmatic particle images.

MicroSIG provides realistic particle images for testing PIV/PTV methods involving defocusing or astigmatic particle images. This includes for instance micro-PIV experiments or 3D PTV methods using defocusing or astigmatism to retrieve the out-of-plane particle position. MicroSIG is based on an approximated model of spherical lens and simple ray tracing.

More detail about the MicroSIG can be found in "M. Rossi, *Synthetic image generator for defocusing and astigmatic PIV/PTV*, Meas. Sci. Technol., **31**, 017003 (2020) [DOI:10.1088/1361-6501/ab42bb](https://doi.org/10.1088/1361-6501/ab42bb)".


### How it works:
1. Install MicroSIG: Download the package and copy the content in a local directory (optional: add the local directory to your preference path in Matlab). 
2. Run MicroSIG: From your local directory, run the file `microsig.m` (or type `microsig` in the command line if the directory was added to your preference path).
3. Follow the instructions and select: the *setting file* (`.txt`), the *data files* (`.txt`), and the *destination folder* for the images.
4. The images in `.tif` format will be created in the *destination folder*.

### Example 1: Poiseuille flow
1. Run microsig.
2. *Setting file*: Select `settings-1.txt` from the folder `\examples` (Alternatively, use `settings-1a.txt` to create images with an astigmatic aberration).
3. *Image files*: Select `example1_poiseuille_flow.txt` from the folder `\examples\`.
4. Select a *destination folder* for your synthetic images.

### Example 2: Rotating spheroids
1. Run MicroSIG.
2. *Setting file*: Select `settings-2.txt` from the folder `\examples`. 
3. *Image files*: Select `example2_rotating_spheroid.txt` from the folder `\examples`.
4. Select a *destination folder* for your synthetic images.

### Setting files:
Setting files must be csv file (`.txt`) in `ASCII` format, each row must be a comma-separated line with: `parameter, value`.
MicroSIG uses 17 parameters:
* `magnification`: Magnification of the simulated lens.
* `numerical_aperture`: Numerical aperture of the simulated lens.
* `focal_length`: This parameter must be chosen empirically from comparison with real images. A typical value is 350. 
* `ri_medium`: Refractive index of the immersion medium of the lens, normally 1.
* `ri_lens`: Refractive index of the immersion medium of the lens, normally 1.5
* `pixel_size`: Size of the side of a square pixel (in microns).
* `pixel_dim_x`: Number of pixels in the x-direction of the sensor.
* `pixel_dim_y`: Number of pixels in the y-direction of the sensor.
* `background_mean`: Constant value of the image background.
* `background_noise`: Amplitude of Gaussian noise added to the images.
* `points_per_pixel`: Number of point sources in a particle, normalized for the particle area. Decrease this parameter for large particles. Typical values are between 10 and 20. 
* `n_rays`: Number of rays for point source. Decrease this value to speed up the particle computation. Typical values are between 100 and 500.
* `gain`: Additional gain to increase or decrease the image intensity.
* `cyl_focal_length`: Additional parameter for astigmatic imaging. This parameter must be chosen empirically from comparison with real images. A typical value is 4000. When it is set to 0 no astigmatism is present. 
* `z_min`: Minimum depth coordinate (in µm), corresponding to z=0 (see next paragraph).
* `z_depth`: Total depth of the measurement volume (in µm).
* `dp`: Diameter of tracer particles (in µm). 

For examples/templates see the files `settings-1.txt`, `settings-1a.txt`, `settings-2.txt` in the `\examples` folder.

### Data files:
Data files must be csv files (`.txt`) in `ASCII` format. A header containing the variable names must be included and the values in each row must be comma-separated. Each column represents a variable, each row represents one particle. Four variables are required: 
* `fr`: number of frame in which the particle is contained.
* `X`: horizontal particle coordinate (in pixels).
* `Y`: vertical particle coordinate (in pixels).
* `Z`: out-of-plane coordinate in normalized units with 0 correpsonding to `z_min` and 1 corresponding to `z_min`+`z_depth`.

Optional variables:
* `dp`: particle diameter (in µm). It can be used to have non-monodispersed particles. If this value is omitted, the `dp` in the setting file is used.
* `c_int`: multiplication factor for the particle image intensity (simulating not-uniform illumination). 
* `ep`: elongation factor for spheroidal particles (vertical axis/horizontal axis)
* `alpha`, `beta`: orientation angles for non-spherical particles (these values are required if `ep` is included).

For examples/templates see the files `example1_poiseuille_flow.txt` and `example2_rotating_spheroid.txt` in the folder `\examples\`.



