function microsig(varargin)
% MICROSIG    Function to generate synthetic particle images
%
%   MICROSIG starts the procedure for generating synthetic images. The
%   first step is to load the setting file (must be a csv file). The second
%   step is to load the coordinate file (must be a csv file, see readme.md
%   and example for more information). The third step is to select
%   the destination folder of the images. Finally the corresponding
%   synthetic images are generated. Images are names as B00001.tif,
%   B00002.fit and so on.
%
%   MICROSIG(my_setting, my_coordinate, my_destination_folder) create the
%   images in my_destination using my_setting as setting
%   and my_coordinate as coordinate. my_setting/my_coordinate can be given
%   in a form of a csv file or of a MATLAB table.
%
%   MICROSIG(my_setting, my_coordinate) same as above but this time the
%   images in the MATLAB workspace.

if nargin==2
    name_mic = varargin{1};
    name_dat = varargin{2};
    folder_images = [];
elseif nargin==3
    name_mic = varargin{1};
    name_dat = varargin{2};
    folder_images = varargin{3};
else
    [filename, pathname] = uigetfile('*.txt','select settings (*.txt)');
    if filename==0, return, end
    name_mic = [pathname,filename];
    
    [filename, pathname] = uigetfile('*.txt','Select source data (*.txt)');
    if filename==0, return, end
    name_dat = [pathname,filename];
    
    folder_images = uigetdir('','Select destination folder');
    if folder_images==0, return, end
end

if ischar(name_dat)
    dat = readtable(name_dat);
elseif istable(name_dat)
    dat = name_dat;
else
    disp('Error: Data file not compatible.')
end

if ischar(name_mic)
    mic = readtable(name_mic,'ReadRowNames',true);
elseif istable(name_mic)
    mic = name_mic;
else
    disp('Error: Settings file not compatible.')
end

if height(mic)==17
    mic = table2struct(rows2vars(mic));
else
    disp('Error: Settings file not compatible.')
    return
end

N_tot = height(dat);
P = zeros(N_tot,4);
fr = dat.fr;
P(:,1) = dat.X;
P(:,2) = dat.Y;
P(:,3) = dat.Z*mic.z_depth+mic.z_min;
if ismember('dp',dat.Properties.VariableNames)
    P(:,4) = dat.dp;
else
    P(:,4) = mic.dp;
end
if ismember('ep',dat.Properties.VariableNames)
    P = [P, dat.ep, dat.alpha, dat.beta];
end
if ismember('c_int',dat.Properties.VariableNames)
    P = [P, dat.c_int];
end


frames = unique(fr);
n_images = length(frames);

n_particles = 0;
tic
for ii = 1:n_images
    disp(['creating image ',num2str(ii),' of ',num2str(n_images),' ...'] )
    flag = fr==frames(ii);
    pos = P(flag,:);
    im_test = take_image(mic,pos);
    n_particles = n_particles+size(pos,1);
    if isempty(folder_images)
        assignin('base',['B',num2str(frames(ii),'%05d')],im_test)
    else
        imwrite(uint16(im_test),[folder_images,'//B',num2str(frames(ii),'%05d'),'.tif'])
    end
end
time_tot = toc;
disp(['total time: ',num2str(time_tot),' sec'])
disp(['particles per images: ',num2str(n_particles/n_images)])
disp(['time per particle: ',num2str(time_tot/n_particles),' sec'])






%%
% 
% % if ~iscell(files_data), dum = files_data; files_data = cell(1,1); files_data{1} = dum; end
% % if ~isempty(folder_images), folder_images(end+1) = '\'; end
% %
% % dumm = importdata([folder_data,files_data{1}]);
% % if isstruct(dumm)
% %     which_version = 'v1.0.0+';
% % else
% %     which_version = 'v0.1.1';
% % end
% %
% 
% % load microscope settings
% file_id = fopen(name_mic);
% data_mic = textscan(file_id,'%s');
% fclose(file_id);
% data_mic = data_mic{1};
% 
% if length(data_mic)==51
%     for ii = 1:3:49
%         eval(['mic.',data_mic{ii},' = ',data_mic{ii+2},';'])
%     end
% elseif length(data_mic)==42
%     for ii = 1:3:40
%         eval(['mic.',data_mic{ii},' = ',data_mic{ii+2},';'])
%         if strcmp(which_version,'v1.0.0+')
%             disp('Error: Settings file not compatible.')
%             return
%         end
%     end
% else
%     disp('Error: Wrong settings file.')
%     return
% end
% 
% 
% if strcmp(which_version,'v1.0.0+')
%     
%     for ii = 1:length(files_data)
%         curr_folder = [folder_images,files_data{ii}(1:end-4),'\'];
%         mkdir(curr_folder);
%         disp(['Creating ',files_data{ii},' ...'])
%         dat = dtracker_create('import_ascii',[folder_data,files_data{ii}]);
%         dat = dtracker_postprocess('unscale',dat);
%         frames = unique(dat.fr);
%         fr = dat.fr(:);
%         P = [dat.x(:), dat.y(:), dat.z(:)*(mic.z_max-mic.z_min)+mic.z_min];
%         if isfield(dat,'dp')
%             P = [P, dat.dp(:)];
%         else
%             P = [P, fr*0+mic.dp];
%         end
%         if isfield(dat,'c_int') && ~isfield(dat,'ep')
%             P = [P, dat.c_int(:)];
%         elseif ~isfield(dat,'c_int') && isfield(dat,'ep')
%             P = [P, dat.ep(:), dat.alpha(:), dat.beta(:)];
%         elseif isfield(dat,'c_int') && isfield(dat,'ep')
%             P = [P, dat.ep(:), dat.alpha(:), dat.beta(:), dat.c_int(:)];
%         end
%         
%         n_images = length(frames);
%         tic
%         n_particles = 0;
%         for jj = 1:n_images
%             disp(['creating image ',num2str(jj),' of ',num2str(n_images),' ...'] )
%             flag = fr==frames(jj);
%             pos = P(flag,:);
%             im_test = take_image(mic,pos);
%             n_particles = n_particles+size(pos,1);
%             imwrite(uint16(im_test),[curr_folder,'B',num2str(jj,'%05d'),'.tif'])
%         end
%         time_tot = toc;
%         disp(['total time: ',num2str(time_tot),' sec'])
%         disp(['particles per images: ',num2str(n_particles/n_images)])
%         disp(['time per particle: ',num2str(time_tot/n_particles),' sec'])
%         
%     end
%     
%     
% elseif strcmp(which_version,'v0.1.1')
%     disp('Warning: Using version v0.1.1.')
%     n_images = length(files_data);
%     tic
%     n_particles = 0;
%     for ii = 1:n_images
%         disp(['creating image ',num2str(ii),' of ',num2str(n_images),' ...'] )
%         pos = importdata([folder_data,files_data{ii}]);
%         im_test = take_image(mic,pos);
%         n_particles = n_particles+size(pos,1);
%         imwrite(uint16(im_test),[folder_images,files_data{ii}(1:end-3),'tif'])
%     end
%     time_tot = toc;
%     disp(['total time: ',num2str(time_tot),' sec'])
%     disp(['particles per images: ',num2str(n_particles/n_images)])
%     disp(['time per particle: ',num2str(time_tot/n_particles),' sec'])
%     
% end
% 
% 
% 
% 
% 
