function xp = create_ellipsoid(Deab,Ns,Nr)

D = Deab(1); ecc = Deab(2); alpha = Deab(3); beta = Deab(4);

R = D/2;

V = spiral_sphere(Ns*2);

V = R*V;
V(3,:) = V(3,:)*ecc; 
R_beta = [cos(beta) 0 sin(beta) ; 0 1 0; -sin(beta) 0 cos(beta)];
R_alpha = [cos(alpha) -sin(alpha) 0; sin(alpha) cos(alpha) 0; 0 0 1];
Vf = (R_alpha*R_beta*V)';

ii1 = find(Vf(:,2)==min(Vf(:,2)),1);
ii2 = find(Vf(:,2)==max(Vf(:,2)),1);
ii3 = find(Vf(:,3)==min(Vf(:,3)),1);
ii4 = find(Vf(:,3)==max(Vf(:,3)),1);
Vdum = Vf([ii1 ii2 ii3 ii4],:);

ft = fittype( 'poly11' );
fdum = fit([Vdum(:,2), Vdum(:,3)], Vdum(:,1), ft );
V1dum = feval(fdum,[Vf(:,2) Vf(:,3)]);
ind = (Vf(:,1)-V1dum)<0;
%             %%
x = Vf(ind,1)';
y = Vf(ind,2)';
z = Vf(ind,3)';
Ns = length(z);

V0 = spiral_sphere(Nr);
u = repmat(x,Nr,1);
v = repmat(y,Nr,1);
s = u*0;
t = u*0;

phs = random('uniform',-pi,pi,1,length(z));
cs = cos(phs);
sn = sin(phs);
for k = 1:Ns     
    V = [cs(k) -sn(k) 0; sn(k) cs(k) 0; 0 0 1]*V0;
    V(1,:) = -abs(V(1,:));
    s(:,k) = V(2,:)./V(1,:);
    t(:,k) = V(3,:)./V(1,:);
    u(:,k) = y(k)-s(:,k)*x(k);
    v(:,k) = z(k)-t(:,k)*x(k);
end
xp = [reshape(u,1,Nr*Ns); reshape(v,1,Nr*Ns);...
    reshape(s,1,Nr*Ns); reshape(t,1,Nr*Ns);];


function V = spiral_sphere(N)

gr = (1+sqrt(5))/2;       % golden ratio
ga = 2*pi*(1-1/gr);       % golden angle

ind_p = 0:(N-1);              % particle (i.e., point sample) index
lat = acos(1-2*ind_p/(N-1));  % latitude is defined so that particle index is proportional to surface area between 0 and lat
lon = ind_p*ga;               % position particles at even intervals along longitude

% Convert from spherical to Cartesian co-ordinates
x = sin(lat).*cos(lon);
y = sin(lat).*sin(lon);
z = cos(lat);
V = [x; y; z];


