clc, clear, close all

tot_toc = 0;

mic.magnification = 10;
mic.numerical_aperture = 0.3;
mic.focal_length = 350;
mic.pixel_size = 6.45;
mic.pixel_dim_x = 512;
mic.pixel_dim_y = 64;
mic.background_mean = 335;
mic.background_noise = 10;
mic.gain = 3.2;
mic.ri_medium = 1;
mic.ri_lens = 1.5;
mic.points_per_pixel = 18;
mic.n_rays = 500;
mic.cyl_focal_length = 0;

%% test1: x, y, z, dp - dp uniform

n_particles = 12;
% define particle coordinates and size
X = linspace(40,472,n_particles);
Y = zeros(1,n_particles)+32;
Z = linspace(-60,20,n_particles);
dp = X*0+2;
tic
im1 = take_image(mic,[X; Y; Z; dp]');
dum_toc = toc;
disp(['test1: ',num2str(dum_toc),' sec'] )
tot_toc = tot_toc+dum_toc;

%% test2: x, y, z, dp - dp non uniform

n_particles = 12;
% define particle coordinates and size
X = linspace(40,472,n_particles);
Y = zeros(1,n_particles)+32;
Z = zeros(1,n_particles)-20;
dp = linspace(1,4,n_particles);
tic
im2 = take_image(mic,[X; Y; Z; dp]');
dum_toc = toc;
disp(['test2: ',num2str(dum_toc),' sec'] )
tot_toc = tot_toc+dum_toc;

%% test3: x, y, z, dp, Ip 

n_particles = 12;
% define particle coordinates and size
X = linspace(40,472,n_particles);
Y = zeros(1,n_particles)+32;
Z = zeros(1,n_particles)-20;
dp = zeros(1,n_particles)+2;
Ip = linspace(0.1,1,n_particles);
tic
im3 = take_image(mic,[X; Y; Z; dp; Ip]');
dum_toc = toc;
disp(['test3: ',num2str(dum_toc),' sec'] )
tot_toc = tot_toc+dum_toc;

%% test4: x, y, z, dp, alpha, beta 

mic.n_rays = 250;
n_particles = 12;
% define particle coordinates and size
X = linspace(40,472,n_particles);
Y = zeros(1,n_particles)+32;
Z = zeros(1,n_particles)-20;
dp = zeros(1,n_particles)+2;
ep = zeros(1,n_particles)+4;
alpha = [0, 0, 0, 0:pi/8:pi/2, pi/2, pi/2, pi/2, pi/2];
beta = [0:pi/8:pi/2, pi/2, pi/2, pi/2:-pi/8:0];
tic
im4 = take_image(mic,[X; Y; Z; dp; ep; alpha; beta;]');
dum_toc = toc;
disp(['test4: ',num2str(dum_toc),' sec'] )
tot_toc = tot_toc+dum_toc;

%% test5: x, y, z, dp, alpha, beta, Ip
tic
im5 = take_image(mic,[X; Y; Z; dp; ep; alpha; beta; Ip]');
dum_toc = toc;
disp(['test5: ',num2str(dum_toc),' sec'] )
tot_toc = tot_toc+dum_toc;

%% test6: x, y, z, dp, astigmatism

mic.n_rays = 500;
mic.cyl_focal_length = 4000;
X = linspace(40,472,n_particles);
Y = zeros(1,n_particles)+32;
Z = linspace(-60,20,n_particles);
dp = X*0+2;
tic
im6 = take_image(mic,[X; Y; Z; dp]');
dum_toc = toc;
disp(['test6: ',num2str(dum_toc),' sec'] )
tot_toc = tot_toc+dum_toc;

%%

im = [im1; im2; im3; im4; im5; im6];
imagesc(im), daspect([1 1 1])
disp(['Totale time: ',num2str(tot_toc),' sec'] )
